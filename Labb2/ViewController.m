//
//  ViewController.m
//  Labb2
//
//  Created by IT-Högskolan on 2015-01-26.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import "ViewController.h"
#import "StoryHandler.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *storyTextView;
@property (weak, nonatomic) IBOutlet UISlider *horrorSlider;
@property (weak, nonatomic) IBOutlet UISlider *positivitySlider;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderControl;

@end

@implementation ViewController
@class StoryHandler;

- (IBAction)generate:(id)sender {
    StoryHandler *sh = [[StoryHandler alloc] init];
    self.storyTextView.text = [sh story:self.genderControl.selectedSegmentIndex withHorror:self.horrorSlider.value andPositivity:self.positivitySlider.value];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.horrorSlider.value = 0;
    self.positivitySlider.value = 0;
    self.genderControl.selected = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
