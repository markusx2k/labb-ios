//
//  StoryHandler.h
//  Labb2
//
//  Created by IT-Högskolan on 2015-01-28.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoryHandler : NSObject
-(NSString*)story: (NSInteger) gender withHorror: (float)horror andPositivity: (float)positivty;
@end
