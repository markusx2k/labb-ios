//
//  StoryHandler.m
//  Labb2
//
//  Created by IT-Högskolan on 2015-01-28.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import "StoryHandler.h"

@interface StoryHandler ()
@property (nonatomic) NSArray *characters;
@property (nonatomic) NSArray *horrorCharacters;
@property (nonatomic) NSArray *girlNames;
@property (nonatomic) NSArray *boyNames;
@property (nonatomic) NSArray *opinions;
@property (nonatomic) NSArray *positiveOpinions;
@property (nonatomic) NSArray *positiveFeelings;
@property (nonatomic) NSArray *feelings;
@property (nonatomic) NSArray *occupation;
@property (nonatomic) NSArray *places;
@property (nonatomic) NSArray *horrorPlaces;
@property (nonatomic) NSArray *adjectives;
@property (nonatomic) NSArray *nouns;

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *heShe;
@property (nonatomic) NSString *hisHer;

@end

@implementation StoryHandler {
    NSArray *characterss;
}

-(void)setStoryWords {
    self.characters = @[@"a viking", @"an ant", @"a snowman"];
    self.horrorCharacters = @[@"a zombie", @"a monster"];
    self.boyNames = @[@"Charles", @"Bob"];
    self.girlNames = @[@"Sara", @"Julia"];
    self.occupation = @[@"a doctor", @"a teacher", @"an archaeologist"];
    self.opinions = @[@"hated", @"disliked"];
    self.positiveOpinions = @[@"liked", @"loved"];
    self.feelings = @[@"scared", @"cold"];
    self.positiveFeelings = @[@"happy", @"warm"];
    self.places = @[@"pub", @"lake", @"park", @"mall"];
    self.horrorPlaces = @[@"haunted house", @"graveyard"];
    self.adjectives = @[@"glowing", @"incredible", @"amazing", @"grand", @"mysterious", @"wierd", @"strange"];
    self.nouns = @[@"a boat", @"a newspaper", @"a gun", @"a sweater"];
}



-(NSString*)story: (NSInteger) gender withHorror: (float)horror andPositivity: (float)positivty {
    [self setStoryWords];
    if (gender == 0) {
        self.name = [self randomElement:self.boyNames];
        self.heShe = @"he";
        self.hisHer = @"his";
    } else {
        self.name = [self randomElement:self.girlNames];
        self.heShe = @"she";
        self.hisHer = @"her";
    }
    
    NSString *character = [self randomElement:self.characters withChance:horror andSpecialArray:self.horrorCharacters];
    NSString *place = [self randomElement:self.places withChance:horror andSpecialArray:self.horrorPlaces];
    
    NSMutableArray *feelings = [self randomArray:self.feelings withAmount:(2) andChance:(positivty) andSpecialArray:self.positiveFeelings];
    NSMutableArray *opinions = [self randomArray:self.opinions withAmount:(2) andChance:(positivty) andSpecialArray:self.positiveOpinions];
    
    NSString *occupation = [self randomElement:self.occupation];
    NSMutableArray *adjectives = [self randomArray:self.adjectives withAmount: 2];
    NSMutableArray *nouns = [self randomArray:self.nouns withAmount:2];
    
    
    NSString *story = [NSString stringWithFormat:@"Once upon a time there was %@ named %@. %@ worked as %@ and %@ %@ %@ job. After work %@ always went to the %@. After visiting the %@ %@ headed home. On %@ way home %@ stumbeled upon something %@. It was %@. %@ had never seen %@ before. It made %@ feel %@. %@ thought it would be best to continue home. %@ lived in a normal home but %@ %@ it. When %@ went to sleep that night %@ dreamt of %@. It was %@ and when %@ woke up %@ felt %@.", character, self.name, self.name, occupation, self.heShe, opinions[0], self.hisHer, self.name, place, place, self.heShe, self.hisHer, self.heShe, adjectives[0], nouns[0], self.name, nouns[0], self.name, feelings[0], self.name, self.name, self.heShe, opinions[1], self.heShe, self.heShe, nouns[1], adjectives[1], self.heShe, self.heShe, feelings[1]];
    
    
    return story;
}
-(NSString*)randomElement: (NSArray*)array {
    return array[arc4random() % array.count];
}

-(NSString*)randomElement: (NSArray*)array withChance: (float)chance andSpecialArray: (NSArray*)specialArray {
    if (((float)rand() / RAND_MAX) < chance) {
        return [self randomElement:specialArray];
    } else {
        return [self randomElement:array];
    }
}

-(NSMutableArray*)randomArray: (NSArray*)array withAmount: (int)amount andChance: (float)chance andSpecialArray: (NSArray*)specialArray {
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < amount; i++) {
        if (((float)rand() / RAND_MAX) < chance) {
            [newArray addObject:[self randomElement:specialArray]];
        } else {
            [newArray addObject:[self randomElement:array]];
        }
    }
    return newArray;
}

-(NSMutableArray*)randomArray: (NSArray*)array withAmount: (int)amount {
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < amount; i++) {
        [newArray addObject:[self randomElement:array]];
    }
    return newArray;
}

@end
